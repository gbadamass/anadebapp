import React, { userState, userEffect } from 'react';
import { SafeAreaView, View, FlatList, StyleSheet, Text, StatusBar } from 'react-native';
import Item from '../components/Item';

const TabOneScreen = () => {
  const [users, Setusers] = userState([]);
  
  userEffect(() =>{
    getUsers();
  }, []);

  const getUsers = () =>{
        fetch('https://api.spacexdata.com/v3/launches/upcoming').then(function(response){
        return response.json();
        }).then(function(response.data) {
         Setusers(response.data);
        });

  }

  const renderItem =  ({ item }) => (
    <Item title={item.mission} />
  );

  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={users}
        renderItem={renderItem}
        keyExtractor={item => item.id.toSrting()}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
});

export default TabOneScreen;
